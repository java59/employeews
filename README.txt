1. Software
   - JDK1.8
   - eclipse
   - WebSphere Application Server(WAS-9)

2. run path url

SOAP
http://<Host>:<Port>/EmployeeWS/services

REST API
 - Method GET = http://<Host>:<Port>/EmployeeWS/empservices/all
 - Method GET = http://<Host>:<Port>/EmployeeWS/empservices/<id>
 - Method POST = http://<Host>:<Port>/EmployeeWS/empservices/post
 - Method PUT = http://<Host>:<Port>/EmployeeWS/empservices/put
 - Method DELETE = http://<Host>:<Port>/EmployeeWS/empservices/<id>