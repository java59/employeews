package com.util;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import com.controller.EmployeeController;

@ApplicationPath("/*")
public class RestApplication extends Application {
	
	public Set<Class<?>> getClasses() {
		Set<Class<?>> classes = new HashSet<Class<?>>();
		classes.add(EmployeeController.class);
		return classes;
	}
}
