package com.soap;

import java.util.List;
import com.bean.EmployeeBean;
import com.dao.EmployeeDao;

// http://<Host>:<Port>/EmployeeWS/services
public class EmployeeService {
	
	public EmployeeBean getEmployeeId(String id){
		EmployeeDao dao = new EmployeeDao();
		EmployeeBean empBean = dao.selectEmployeetById(id);
	return empBean;
	}
	
	public EmployeeBean[] getEmployeeAll(){
		EmployeeDao dao = new EmployeeDao();
		List<EmployeeBean> empList  = dao.selectEmployeetAll();
	return empList.toArray(new EmployeeBean[0]);
	}
	
	public EmployeeBean addEmployee(EmployeeBean emp){
		EmployeeDao dao = new EmployeeDao();
		EmployeeBean empBean = dao.insertEmployee(emp);	
	return empBean;
	}
	
	public String changEmployeeById(EmployeeBean emp){
		EmployeeDao dao = new EmployeeDao();
		String result = dao.updateEmployeetById(emp);
	return result;
	}
	
	public String removeEmployeeId( String id){	
		EmployeeDao dao = new EmployeeDao();
		String result = dao.deleteEmployeetById(id);
		return result;
	}
}
