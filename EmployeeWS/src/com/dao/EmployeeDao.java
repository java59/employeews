package com.dao;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.xml.ws.WebServiceContext;
import com.bean.EmployeeBean;
import com.util.StringUtil;

public class EmployeeDao {
	@Resource
	private WebServiceContext context;

	static List<EmployeeBean> empList = new ArrayList<>();

	public EmployeeBean selectEmployeetById(String id) {
		EmployeeBean empBean = new EmployeeBean();
		try {
			int empId = Integer.parseInt(id);
			for (EmployeeBean emp : empList) {
				if (emp.getId().equals(empId)) {
					empBean.setId(emp.getId());
					empBean.setIdCard(emp.getIdCard());
					empBean.setfName(emp.getfName());
					empBean.setlName(emp.getlName());
					empBean.setEmail(emp.getEmail());
					empBean.setPhone(emp.getPhone());
				}
			}
		} catch (Exception ex) {
			System.out.println("ERROR MESSAG :" + ex.getMessage());
		}
		return empBean;
	}

	public List<EmployeeBean> selectEmployeetAll() {
		return empList;
	}

	public EmployeeBean insertEmployee(EmployeeBean empOjb) {
		EmployeeBean empBean = new EmployeeBean();
		try {
			empBean.setId(StringUtil.next());
			empBean.setIdCard(empOjb.getIdCard());
			empBean.setfName(empOjb.getfName());
			empBean.setlName(empOjb.getlName());
			empBean.setEmail(empOjb.getEmail());
			empBean.setPhone(empOjb.getPhone());
			empList.add(empBean);
		} catch (Exception ex) {
			System.out.println("ERROR MESSAG :" + ex.getMessage());
		}
		return empBean;
	}

	public String updateEmployeetById(EmployeeBean empOjb) {
		String result = null;
		try {
			for (int i = 0; i < empList.size(); i++) {
				if (empList.get(i).getId().equals(empOjb.getId())) {
					empList.get(i).setId(empOjb.getId());
					empList.get(i).setIdCard(empOjb.getIdCard());
					empList.get(i).setfName(empOjb.getfName());
					empList.get(i).setlName(empOjb.getlName());
					empList.get(i).setEmail(empOjb.getEmail());
					empList.get(i).setPhone(empOjb.getPhone());
					result = "UPDATE IS SUCCESS.";
					break;
				} else {
					result = "ID NOT FOUND";
				}
			}
		} catch (Exception ex) {
			System.out.println("ERROR MESSAG :" + ex.getMessage());
		}
		return result;
	}

	public String deleteEmployeetById(String id) {
		String result = null;
		try {
			int empId = Integer.parseInt(id);
			for (EmployeeBean emp : empList) {
				if (empId == emp.getId()) {
					empList.remove(emp);
					result = "DEELETE ID:" + empId + " IS SUCCESS";
					break;
				} else {
					result = "ID NOT FOUND ";
				}
			}
		} catch (Exception ex) {
			System.out.println("ERROR MESSAG :" + ex.getMessage());
			result = "ERROR MESSAG :" + ex.getMessage();
		}
		return result;
	}
}
