package com.controller;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.bean.EmployeeBean;
import com.dao.EmployeeDao;

@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })

// http://<Host>:<Port>/EmployeeWS/empservices/XX
@Path("/empservices")
public class EmployeeController {
	EmployeeDao dao = new EmployeeDao();
	
	@GET
	@Path("{id}")
	public Response getEmployeeId(@PathParam("id") String id){
		EmployeeBean empBean = dao.selectEmployeetById(id);
		return Response.status(Response.Status.OK).entity(empBean).build();
	}

	@GET
	@Path("/all")
	public Response getEmployeeAll(){
		List<EmployeeBean> empList  = dao.selectEmployeetAll();
		return Response.status(Response.Status.OK).entity(empList).build();
	}
	
	@POST
	@Path("/post")
	public Response addEmployee(EmployeeBean emp){
		EmployeeBean empBean = dao.insertEmployee(emp);	
		return Response.status(Response.Status.ACCEPTED).entity(empBean).build();
	}
	
	@PUT
	@Path("/put")
	public Response changEmployeeById(EmployeeBean emp){
		String result = dao.updateEmployeetById(emp);
		return Response.status(Response.Status.OK).entity(result).build();
	}
	
	@DELETE
	@Path("{id}")
	public Response removeEmployeeId(@PathParam("id") String id){	
		String result = dao.deleteEmployeetById(id);
		return Response.status(Response.Status.OK).entity(result).build();
	}
}
