
package com.soap.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "addEmployeeResponse", namespace = "http://soap.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addEmployeeResponse", namespace = "http://soap.com/")
public class AddEmployeeResponse {

    @XmlElement(name = "return", namespace = "")
    private com.bean.EmployeeBean _return;

    /**
     * 
     * @return
     *     returns EmployeeBean
     */
    public com.bean.EmployeeBean getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(com.bean.EmployeeBean _return) {
        this._return = _return;
    }

}
